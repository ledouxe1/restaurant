require 'test_helper'

class CookedStatusesControllerTest < ActionController::TestCase
  setup do
    @cooked_status = cooked_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cooked_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cooked_status" do
    assert_difference('CookedStatus.count') do
      post :create, cooked_status: { desc: @cooked_status.desc }
    end

    assert_redirected_to cooked_status_path(assigns(:cooked_status))
  end

  test "should show cooked_status" do
    get :show, id: @cooked_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cooked_status
    assert_response :success
  end

  test "should update cooked_status" do
    put :update, id: @cooked_status, cooked_status: { desc: @cooked_status.desc }
    assert_redirected_to cooked_status_path(assigns(:cooked_status))
  end

  test "should destroy cooked_status" do
    assert_difference('CookedStatus.count', -1) do
      delete :destroy, id: @cooked_status
    end

    assert_redirected_to cooked_statuses_path
  end
end
