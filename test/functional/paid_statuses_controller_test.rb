require 'test_helper'

class PaidStatusesControllerTest < ActionController::TestCase
  setup do
    @paid_status = paid_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paid_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paid_status" do
    assert_difference('PaidStatus.count') do
      post :create, paid_status: { desc: @paid_status.desc }
    end

    assert_redirected_to paid_status_path(assigns(:paid_status))
  end

  test "should show paid_status" do
    get :show, id: @paid_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paid_status
    assert_response :success
  end

  test "should update paid_status" do
    put :update, id: @paid_status, paid_status: { desc: @paid_status.desc }
    assert_redirected_to paid_status_path(assigns(:paid_status))
  end

  test "should destroy paid_status" do
    assert_difference('PaidStatus.count', -1) do
      delete :destroy, id: @paid_status
    end

    assert_redirected_to paid_statuses_path
  end
end
