class PaidStatusesController < ApplicationController
  # GET /paid_statuses
  # GET /paid_statuses.json
  def index
    @paid_statuses = PaidStatus.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @paid_statuses }
    end
  end

  # GET /paid_statuses/1
  # GET /paid_statuses/1.json
  def show
    @paid_status = PaidStatus.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @paid_status }
    end
  end

  # GET /paid_statuses/new
  # GET /paid_statuses/new.json
  def new
    @paid_status = PaidStatus.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @paid_status }
    end
  end

  # GET /paid_statuses/1/edit
  def edit
    @paid_status = PaidStatus.find(params[:id])
  end

  # POST /paid_statuses
  # POST /paid_statuses.json
  def create
    @paid_status = PaidStatus.new(params[:paid_status])

    respond_to do |format|
      if @paid_status.save
        format.html { redirect_to @paid_status, notice: 'Paid status was successfully created.' }
        format.json { render json: @paid_status, status: :created, location: @paid_status }
      else
        format.html { render action: "new" }
        format.json { render json: @paid_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /paid_statuses/1
  # PUT /paid_statuses/1.json
  def update
    @paid_status = PaidStatus.find(params[:id])

    respond_to do |format|
      if @paid_status.update_attributes(params[:paid_status])
        format.html { redirect_to @paid_status, notice: 'Paid status was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @paid_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paid_statuses/1
  # DELETE /paid_statuses/1.json
  def destroy
    @paid_status = PaidStatus.find(params[:id])
    @paid_status.destroy

    respond_to do |format|
      format.html { redirect_to paid_statuses_url }
      format.json { head :no_content }
    end
  end
end
