class CookedStatusesController < ApplicationController
  # GET /cooked_statuses
  # GET /cooked_statuses.json
  def index
    @cooked_statuses = CookedStatus.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @cooked_statuses }
    end
  end

  # GET /cooked_statuses/1
  # GET /cooked_statuses/1.json
  def show
    @cooked_status = CookedStatus.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cooked_status }
    end
  end

  # GET /cooked_statuses/new
  # GET /cooked_statuses/new.json
  def new
    @cooked_status = CookedStatus.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cooked_status }
    end
  end

  # GET /cooked_statuses/1/edit
  def edit
    @cooked_status = CookedStatus.find(params[:id])
  end

  # POST /cooked_statuses
  # POST /cooked_statuses.json
  def create
    @cooked_status = CookedStatus.new(params[:cooked_status])

    respond_to do |format|
      if @cooked_status.save
        format.html { redirect_to @cooked_status, notice: 'Cooked status was successfully created.' }
        format.json { render json: @cooked_status, status: :created, location: @cooked_status }
      else
        format.html { render action: "new" }
        format.json { render json: @cooked_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /cooked_statuses/1
  # PUT /cooked_statuses/1.json
  def update
    @cooked_status = CookedStatus.find(params[:id])

    respond_to do |format|
      if @cooked_status.update_attributes(params[:cooked_status])
        format.html { redirect_to @cooked_status, notice: 'Cooked status was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cooked_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cooked_statuses/1
  # DELETE /cooked_statuses/1.json
  def destroy
    @cooked_status = CookedStatus.find(params[:id])
    @cooked_status.destroy

    respond_to do |format|
      format.html { redirect_to cooked_statuses_url }
      format.json { head :no_content }
    end
  end
end
