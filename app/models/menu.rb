class Menu < ActiveRecord::Base
  attr_accessible :desc, :price

  has_many :tickets, :dependent =>  :destroy
  has_many :orders, :through => :tickets

  validates :desc, presence: true
  validates :price, :numericality => {:greater_than => 0}


      def desc_price
        "#{desc} - $#{price}"
      end

end
