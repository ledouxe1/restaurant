class Order < ActiveRecord::Base
  attr_accessible :paid_staus, :cooked_status, :cashier_name, :cook_name, :cooked_status_id, :num_guests, :paid_status_id, :res_name, :server_name, :ticket_id

  belongs_to :cooked_status
  belongs_to :paid_status

  #has_many :cooked_statuses
  #has_many :paid_statuses

  has_many :tickets, :dependent =>  :destroy
  has_many :menus, :through => :tickets

  validates :res_name, presence: true
  validates :server_name, presence: true
  validates :cook_name, presence: true
  validates :cashier_name, presence: true
  validates :num_guests, :numericality => {:greater_than => 0}


  def sub_total
    value = menus.sum{|menu| menu.price}
  end

  def tax
    value = menus.sum{|menu| menu.price}
    price = (value * 0.07).round(2)
  end

  def total_price
    value = menus.sum{|menu| menu.price}
    price = value +(value * 0.07).round(2)
  end

end
