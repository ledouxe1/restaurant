class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :desc
      t.float :price

      t.timestamps
    end
  end
end
