class CreateCookedStatuses < ActiveRecord::Migration
  def change
    create_table :cooked_statuses do |t|
      t.string :desc

      t.timestamps
    end
  end
end
