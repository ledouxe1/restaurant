class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.references :order
      t.references :menu
      t.float :price

      t.timestamps
    end
  end
end
