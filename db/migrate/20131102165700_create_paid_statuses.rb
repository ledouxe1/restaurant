class CreatePaidStatuses < ActiveRecord::Migration
  def change
    create_table :paid_statuses do |t|
      t.string :desc

      t.timestamps
    end
  end
end
