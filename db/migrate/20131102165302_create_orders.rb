class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :ticket_id
      t.string :res_name
      t.integer :num_guests
      t.string :server_name
      t.string :cook_name
      t.string :cashier_name
      t.references :cooked_status
      t.references :paid_status

      t.timestamps
    end
  end
end
