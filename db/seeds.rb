# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Order.delete_all
Menu.delete_all
Ticket.delete_all
CookedStatus.delete_all
PaidStatus.delete_all


Menu.create([{desc: " Cheese Burger", price: 8.00},{desc: "French Fries", price: 2.50}, {desc: "Pizza", price: 10.00}, {desc: "Salad", price: 5.00}, {desc: "Drink", price: 2.00}])
CookedStatus.create([{desc: "In Progress"},{desc:"Complete"}])
PaidStatus.create([{desc:"Not Paid"},{desc:"Paid"}])